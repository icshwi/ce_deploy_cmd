# Batch operations

This script was written to support batch operations using ce-deploy API. It supports `deployment`, `undeployment`, `start`, `stop` and `register`.

## Requirements

This script depends on [python-gitlab](https://pypi.org/project/python-gitlab/):

```bash
pip install python-gitlab
```

## Usage

```bash
python3 batch_operations.py <config_file> [--phony]
```

## Config file

The main input to the script is a YAML config file with the following structure

```yaml
<action>:
    <global property>: <value>
    iocs:
        - "IOCNAME":
            <local_property> : <value>
```

### IOC Register

To register multiple IOCs at once, one can create a config file like the following.

Note that this script does not manage registering the IOCs into Naming service, so these have to exist there already.

```yaml
register:
    iocs:
    - "CCCE:SC-IOC-110":
        git-repo: https://gitlab.esss.lu.se/ccce/dev/iocs/instances/e3-ioc-test_batch-1
    - "CCCE:SC-IOC-210":
        git-repo: https://gitlab.esss.lu.se/ccce/dev/iocs/instances/e3-ioc-test_batch-2
    - "CCCE:SC-IOC-310":
        git-repo: https://gitlab.esss.lu.se/ccce/dev/iocs/instances/e3-ioc-test_batch-3
    - "CCCE:SC-IOC-410":
        git-repo: https://gitlab.esss.lu.se/ccce/dev/iocs/instances/e3-ioc-test_batch-4
```

### IOC Deployment

For batch deployment, a list of IOCs and the host where to deploy them must be supplied. The IOCs can be specified either by name or by their ID on CE-Deploy (only for operations on existing IOCs).

The following fields can be configured:

* `host` is the FQDN (fully qualified domain name / hostname) of the host where the IOC will be deployed.
    * If the host is not defined, the script will assume the deployment if for the same host that the IOC is currently deployed to and will use that as input.

* `revision` can be set globally for the whole action block and it will apply for every IOC if not explicity listed under the IOC properties.

* `lock` is a boolean that defines the behavior of the deployment actions, defining if it will wait until the current deployment is finished before starting/queueing a new one.

* `chunk` is used to split the actions into smaller blocks. If a size of 4 is used, for example, then the script will queue and send the deployments of 4 IOCs at most at once and wait until these finish to send extra 4.

    * Note that for `chunk` to work as intended, `lock` must be false or not defined. Otherwise the script will block on each deployment action, rendering the chunk meaningless.

```yaml
deploy:
    lock: false
    chunk: 2
    revision: "Version-1"
    iocs:
        - "CCCE:SC-IOC-110":
            host: "ccce-test-ioc-01.cslab.esss.lu.se"
        - "CCCE:SC-IOC-210" # Current deployed to host (if existing) will be used as destination for this deployment
        - "CCCE:SC-IOC-310":
            host: "ccce-test-ioc-01.cslab.esss.lu.se"
            revision: "Version-2" # Overrides global revision for this IOC only
        - "CCCE:SC-IOC-410":
            host: "ccce-test-ioc-01.cslab.esss.lu.se"
```

### IOC Undeployment

See IOC Deployment for an explanation on `chunk` and `lock` options

```yaml
undeploy:
    lock: false
    chunk: 2
    iocs:
        - "CCCE:SC-IOC-110"
        - "CCCE:SC-IOC-210"
        - "CCCE:SC-IOC-310"
        - "CCCE:SC-IOC-410"
```

### IOC Start/Stop

See IOC Deployment for an explanation on `chunk` and `lock` options

```yaml
start:
    iocs:
        - "CCCE:SC-IOC-110"
        - "CCCE:SC-IOC-210"
        - "CCCE:SC-IOC-310"
        - "CCCE:SC-IOC-410"
```

```yaml
stop:
    iocs:
        - "CCCE:SC-IOC-110"
        - "CCCE:SC-IOC-210"
        - "CCCE:SC-IOC-310"
        - "CCCE:SC-IOC-410"
```

## Order of operations

Each YAML config file can have multiple actions listed on the same file. This script will parse them all and execute the actions on the following order from top to bottom:

- Register
- Deploy
- Undeploy
- Start
- Stop

So for a first deployment, your configuration file could list both register and deployment actions together:

```yaml
register:
    iocs:
    - CCCE:SC-IOC-110:
        git-repo: https://gitlab.esss.lu.se/ccce/dev/iocs/instances/e3-ioc-test_batch-1
    - CCCE:SC-IOC-210:
        git-repo: https://gitlab.esss.lu.se/ccce/dev/iocs/instances/e3-ioc-test_batch-2

deploy:
    revision: "Version-1"
    iocs:
        - "CCCE:SC-IOC-110":
            host: "ccce-test-ioc-01.cslab.esss.lu.se"
        - "CCCE:SC-IOC-210":
            host: "ccce-test-ioc-01.cslab.esss.lu.se"
```