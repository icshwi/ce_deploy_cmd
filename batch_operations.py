import yaml
import argparse
from ce_deploy import CCCE, HTTPError, AlreadyDeployedException, NoIOCActiveDeployment
from collections import OrderedDict
from time import sleep

DEFAULT_CHUNK_SIZE = 4
DEFAULT_LOCK = False
CE_DEPLOY_URL = "ce-deploy.esss.lu.se"

parser = argparse.ArgumentParser(description='Perform batch operations with ce-deploy')
parser.add_argument('operation_file', type=argparse.FileType('r'), help='YAML file containing the operations to be performed')
parser.add_argument('--phony', action='store_true', default=False, help='Only print which operations would be performed, do not act')
args = parser.parse_args()

class IOCInfo:
    def __init__(self, ioc_dict):
        if not isinstance(ioc_dict, dict):
            self.name = ioc_dict
            self.repo = ''
            self.host = ''
            self.revision = ''
        else:
            self.name = next(iter(ioc_dict))
            ioc_info = ioc_dict[self.name]
            self.repo = ioc_info.get('git-repo', '')
            self.host = ioc_info.get('host', '')
            self.revision = ioc_info.get('revision', '')

def split_list(lst, chunk_size):
    if chunk_size == 0:
        yield lst
    else:
        for i in range(0, len(lst), chunk_size):
            yield lst[i:i + chunk_size]

def check_ongoing_jobs(jobs):
    if len(jobs) == 0:
        return
    print(f"Checking jobs {jobs} in the background")

    status = {job: False for job in jobs}
    while not all(status.values()):
        for job in jobs:
            status[job] = ccce.operation_is_done(job)
        sleep(10)

def perform_operation(config, operation_type, operation):
    chunk = config.get('chunk', DEFAULT_CHUNK_SIZE)
    lock = config.get('lock', DEFAULT_LOCK)

    global_revision = config.get('revision', '')

    for chunk in split_list(config["iocs"], chunk):
        ongoing_jobs = []
        for ioc_dict in chunk:
            ioc_info = IOCInfo(ioc_dict)
            if operation_type == 'deploy':
                revision = ioc_info.revision or global_revision
                if not ioc_info.host:
                    print("No host specified, assuming the deployment is for the same host")
                host = ioc_info.host or ccce.get_deployed_to_host(ioc_info.name)

                print(f"\n{operation_type.capitalize()} {ioc_info.name}")
                print(f"\tHost: {host}")
                print(f"\tRevision: {revision}")
                if not args.phony:
                    try:
                        job_id = operation(ioc_info.name, host, revision, lock, force_redeploy=True)
                        ongoing_jobs.append(job_id)
                    except (AlreadyDeployedException, HTTPError) as e:
                        print(e)
                        continue
            elif operation_type == 'register':
                print(f"\n{operation_type.capitalize()} {ioc_info.name}")
                print(f"\tGit Repo: {ioc_info.repo}")
                if not args.phony:
                    try:
                        job_id = operation(ioc_info.name, ioc_info.repo)
                        ongoing_jobs.append(job_id)
                    except HTTPError as e:
                        print(f"Failed to register IOC! Error: {e}")
                        continue
            else:
                print(f"\n{operation_type.capitalize()} {ioc_info.name}")
                if not args.phony:
                    try:
                        job_id = operation(ioc_info.name, lock)
                        ongoing_jobs.append(job_id)
                    except (AlreadyDeployedException, NoIOCActiveDeployment, HTTPError) as e:
                        print(f"Failed to {operation_type} IOC! Error: {e}")
                        continue
        check_ongoing_jobs(ongoing_jobs)

try:
    operation_list = yaml.safe_load(args.operation_file)
except Exception as e:
    print(f"Error loading operation file: {e}")
    raise

ccce = CCCE(url=CE_DEPLOY_URL)
if args.phony:
    print("WARNING: --phony flag set. No actions will be performed")

# Operations will be executed following the order in this dict
operations = OrderedDict([
    ("register", ccce.register_ioc),
    ("deploy", ccce.deploy_ioc),
    ("undeploy", ccce.undeploy_ioc),
    ("start", ccce.start_ioc),
    ("stop", ccce.stop_ioc)
])

for operation_name, config in operation_list.items():
    if operation_name in operations:
        perform_operation(config, operation_name, operations[operation_name])
    else:
        print(f"Unknown operation: {operation_name}")
