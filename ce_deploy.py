from time import sleep

import getpass
import gitlab
import os
import requests

from http.client import HTTPConnection
from urllib.parse import quote as urlencode, urlsplit
HTTPConnection.debuglevel = 0


class DeployedToDifferentHostException(Exception):
    def __init__(self, ioc, host):
        super(DeployedToDifferentHostException, self).__init__()
        self.ioc = ioc
        self.host = host


    def __str__(self):
        return f"{self.ioc} deployed to a different host: {self.host}"


class AlreadyDeployedException(Exception):
    def __init__(self, ioc):
        super(AlreadyDeployedException, self).__init__()
        self.ioc = ioc


    def __str__(self):
        return f"{self.ioc} already deployed"


class NoHostFoundException(Exception):
    def __init__(self, host):
        super(NoHostFoundException, self).__init__()
        self.host = host


    def __str__(self):
        return f"Host '{self.host}' not found"


class NoIOCInHostException(Exception):
    def __init__(self, host):
        super(NoIOCInHostException, self).__init__()
        self.host = host


    def __str__(self):
        return f"No IOCs found in host '{self.host}'"


class NoIOCFoundException(Exception):
    def __init__(self, ioc):
        super(NoIOCFoundException, self).__init__()
        self.ioc = ioc


    def __str__(self):
        return f"IOC '{self.ioc}' not found"


class NoIOCActiveDeployment(Exception):
    def __init__(self, ioc):
        super(NoIOCActiveDeployment, self).__init__()
        self.ioc = ioc


    def __str__(self):
        return f"No Active deployment for IOC '{self.ioc['namingName']}' ({self.ioc['id']}) found"


class HTTPError(Exception):
    def __init__(self, result):
        super(HTTPError, self).__init__()
        self.status_code = result.status_code
        try:
            self.reason = result.json()["description"]
        except:
            self.reason = result.reason


    def __str__(self):
        return f"HTTP Error: {self.status_code} {self.reason}"


class CCCE(object):
    def __init__(self, url="ce-deploy.esss.lu.se"):
        super(CCCE, self).__init__()

        self.session = requests.sessions.Session()
        self.cookiejar = requests.cookies.RequestsCookieJar()
        self.url = url
        self.login()
        self.gitlab = gitlab.Gitlab("https://gitlab.esss.lu.se")


    @staticmethod
    def __token_file():
        return os.path.join(os.path.expanduser("~"), ".ccce-api-token")


    def get_gitlab_token(self):
        with open(os.path.join(os.path.expanduser("~"), ".gitlab-api-token"), "rt") as token_file:
            return token_file.readline()


    def __load_token(self):
        with open(self.__token_file(), "rt") as token_file:
            return token_file.readline()


    def __renew_token(self):
        try:
            result = self._post(f"https://{self.url}/api/v1/authentication/renew")
        except HTTPError as e:
            print(e)
            self.__do_login()
            return

        self.__save_token(result["token"])


    def __apply_token(self, token):
        self.access_token = token
        self.cookiejar.set("ce-deploy-auth", token, domain=self.url)


    def __save_token(self, token):
        self.__apply_token(token)
        with open(self.__token_file(), "wt") as token_file:
            os.chmod(self.__token_file(), 0o600)
            return token_file.write(self.access_token)


    def __do_login(self):
        user_name = input('Username: ')
        data={"userName":user_name, "password":getpass.getpass(f"Password for '{user_name}': ")}
        result = self.session.post(f"https://{self.url}/api/v1/authentication/login", headers={ 'Accept': 'application/json' }, json=data)
        if result.status_code < 200 or result.status_code > 299:
            raise HTTPError(result)
        result_json = result.json()
        self.__save_token(result_json["token"])


    def login(self):
        try:
            self.__apply_token(self.__load_token())
        except Exception as e:
            print(e)
            self.__do_login()
            return

        self.__renew_token()


    def _get(self, url, **kwargs):
        result = self.session.get(url, headers={ 'Accept': 'application/json' }, cookies=self.cookiejar, **kwargs)
        if result.status_code != 200:
            raise HTTPError(result)

        return result.json()


    def _post(self, url, **kwargs):
        result = self.session.post(url, headers={ 'Accept': 'application/json' }, cookies=self.cookiejar, **kwargs)
        if result.status_code != 200 and result.status_code != 201:
            raise HTTPError(result)

        return result.json()


    def _get_gitlab_id(self, project):
        try:
            return int(project)
        except:
            pass

        path = urlsplit(project.strip())[2]
        if path.endswith(".git"):
            path = path[:-4]

        # Strip the "/" in the beginning
        if path.startswith("/"):
            path = path[1:]
        project = self.gitlab.projects.get(urlencode(path))
        return int(project.id)


    def get_naming_id(self, iocname):
        try:
            naming_id = int(iocname)
        except:
            naming_id = self._get(f"https://{self.url}/api/v1/names/ioc_names_by_name?ioc_name={iocname.strip()}")
            naming_id = naming_id[0]["uuid"]

        return naming_id


    def get_host_id_by_name(self, hostname):
        try:
            result = self._get(f"https://{self.url}/api/v1/hosts/{hostname.split('.')[0]}/info_by_fqdn")
        except Exception as e:
            raise NoHostFoundException(hostname)

        return result["hostId"]


    def get_deployed_to_host(self, iocname):
        ioc = self.get_ioc(iocname)
        try:
            return ioc['activeDeployment']['host']['fqdn']
        except ValueError:
            raise NoIOCActiveDeployment(ioc)


    def get_iocs_in_host(self, hostname):
        host_id = self.get_host_id_by_name(hostname)
        try:
            iocs = self._get(f"https://{self.url}/api/v1/hosts/{host_id}/iocs")["deployedIocs"]
        except Exception as e:
            iocs = []

        return iocs

    def _get_ioc_by_id(self, iocid):
        return self._get(f"https://{self.url}/api/v1/iocs/{iocid}")


    def _query_ioc(self, query):
        params = {"query": query}
        result = self._get(f"https://{self.url}/api/v1/iocs", params=params)
        if result["totalCount"] == 0:
            raise NoIOCFoundException(query)
        assert result["totalCount"] == 1

        return result["iocList"][0]


    def get_ioc(self, query):
        try:
            iocid = int(query)
        except ValueError:
            return self._query_ioc(query)

        return self._get_ioc_by_id(iocid)


    def get_commits(self, ioc):
        """
        [
          {
            "reference": "0.2.3",
            "shortReference": "0.2.3",
            "description": "",
            "commitDate": null
          },
          {
            "reference": "f6174dc953a03ddce3969214874bf044e1fc5852",
            "shortReference": "f6174dc9",
            "description": "Initial commit\n\nPorted from https://gitlab.esss.lu.se/ioc/e3-ioc-operation\n",
            "commitDate": "2022-01-25T15:33:07.000+00:00"
          }
        ]
        """
        try:
            repo_id = ioc["gitProjectId"]
        except:
            ioc = self.get_ioc(ioc)
            repo_id = ioc["gitProjectId"]

        return self._get(f"https://{self.url}/api/v1/git_helper/{repo_id}/tags_and_commits")


    def register_ioc(self, ioc, repo):
        ioc_id = self.get_naming_id(ioc)

        data = {"namingUuid" : ioc_id, "gitProjectId" : self._get_gitlab_id(repo)}
        result = self._post(f"https://{self.url}/api/v1/iocs", json=data)
        return result['id']


    def deploy_ioc(self, ioc, host, version, lock = True, force_redeploy = False):
        ioc = self.get_ioc(ioc)
        ioc_id = ioc["id"]
        ioc_name = ioc["namingName"]
        host_id = self.get_host_id_by_name(host)

        if version is None:
            version = self.get_commits(ioc)[0]["reference"]

        if ioc.get("activeDeployment"):
            deployed_to = ioc["activeDeployment"]["host"]["hostId"]
            if host_id != deployed_to:
                raise DeployedToDifferentHostException(ioc_name, deployed_to)
            deployed_version = ioc["activeDeployment"]["sourceVersion"]
            if not force_redeploy and (version == deployed_version):
                raise AlreadyDeployedException(ioc_name)

        data = {"hostId": host_id, "sourceVersion": version}
        print(f"Deploying {ioc_name} ({ioc_id}) to {host} ({host_id}) with version {version}")

        result = self._post(f"https://{self.url}/api/v1/jobs/deploy/{ioc_id}", json=data)
        deployment_id = result["id"]

        if lock:
            while True:
                print(f"Checking job {deployment_id} in the background...")
                if not self.operation_is_done(deployment_id):
                    sleep(10)
        else:
            print(f'Deployment queued! Job ID: {deployment_id}')
        return deployment_id


    def operation_is_done(self, id_):
        try:
            result = self._get(f"https://{self.url}/api/v1/jobs/{id_}")
        except HTTPError as error:
            if error.status_code == 404:
                # Not queued yet in awx
                return False
        return result["status"] in ["SUCCESSFUL", "FAILED"]


    def undeploy_ioc(self, ioc, lock = True):
        ioc = self.get_ioc(ioc)
        ioc_id = ioc["id"]
        ioc_name = ioc["namingName"]

        print(f"Undeploying {ioc_name}")
        try:
            result = self._post(f"https://{self.url}/api/v1/jobs/undeploy/{ioc_id}")
        except HTTPError as error:
            if error.status_code == 422:
                raise NoIOCActiveDeployment(ioc)
            raise
        undeployment_id = result["id"]
        if lock:
            while True:
                print(f"Checking job {undeployment_id} in the background...")
                if not self.operation_is_done(undeployment_id):
                    sleep(10)
        else:
            print(f'Undeployment queued! Job ID: {undeployment_id}')
        return undeployment_id


    def __start_stop_ioc(self, ioc, cmd, lock = True):
        ioc = self.get_ioc(ioc)
        ioc_id = ioc["id"]
        ioc_name = ioc["namingName"]

        print(f"{cmd.capitalize()}ing {ioc_name}")
        result = self._post(f"https://{self.url}/api/v1/jobs/{cmd}/{ioc_id}")
        command_id = result["id"]
        if lock:
            while True:
                print(f"Checking job {command_id} in the background...")
                if not self.operation_is_done(command_id):
                    sleep(10)
        else:
            print(f"{cmd.capitalize()} Command queued! Job ID: {command_id}")
        return command_id

    def start_ioc(self, ioc, lock = True):
        return self.__start_stop_ioc(ioc, "start", lock)

    def stop_ioc(self, ioc, lock = True):
        return self.__start_stop_ioc(ioc, "stop", lock)
